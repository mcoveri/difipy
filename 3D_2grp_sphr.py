#!/usr/bin/python

# Finite Volume approach to neutron diffusion in a
# 1D spherical geometry. 2 group approach easily extended
# to N group, provided we have group constants for all groups desired.
#
# William Gurecky
#
# Changelog
# -------------
# 20/12/2014    Initial demonstration of 2 group calculation.
# 1/16/2014     Can now solve in parallel without issue (k is global var)
# 1/17/2014     Use Lamarsh as pseudo benchmark case
#

from fipy import *
from mat_mixer import *
import copy

# CONSTANTS
# -------------
# Number energy groups
nGrp = 2

# Initial guess for flux (phi) and neutron multiplication factor
phi0 = 1.0  # n/cm^2-s
k0 = 1.1

# From Lamarsh Intro to Reactor Theory (pg 336)
# In core:
#   M_h20 = 41.6 kg
#     atom_density_h20 = 3.35e-2 a/b-cm
#   M_U235 = 1.15 kg
#     atom_density_U235 = 6.7e-5 a/b-cm
# Reflector:
#   Pure water (density ~1g/cc ?)
# create the smeared moderator fuel mixture
fuelMat = mixedMat({'U235_2g': 6.7e-5, 'O16_2g': 3.35e-2, 'H1_2g': 3.35e-2 * 2.})
lwtrMat = mixedMat({'O16_2g': 1.0 / 3.0, 'H1_2g': 2.0 / 3.0})
lwtrMat.setDensity(1.0)  # g/cc
modMat = copy.deepcopy(lwtrMat)
coreMat = fuelMat

# Each key in dictionary coorosponds to a region in the mesh.
# Regions in the mesh are denoded as 'interior volumes' in gmsh or maunally
# in FiPy meshes
materialDict = {'core': coreMat, 'reflector': modMat}

# setup the mesh
mesh = Gmsh3D('./gmsh/sphere.gmsh')
# Store locations of mesh centers and faces
rc = mesh.cellCenters
rfc = mesh.faceCenters
print("Number of cells in mesh: %d " % len(rc[0]))

# Define physical mesh regions
coreMask = mesh.physicalCells["core"]
refMask = mesh.physicalCells["reflector"]
regionDict = {'core': coreMask, 'reflector': refMask}

# Setup mesh vars
Sigma_s = CellVariable(name="Scatter Xsec",
                       mesh=mesh,
                       value=[[0.], [0.]],
                       elementshape=(2,))
Sigma_a = CellVariable(name="Abs Xsec",
                       mesh=mesh,
                       value=numerix.array([[0.], [0.]]),
                       elementshape=(2,))
Sigma_f = CellVariable(name="Fission Xsec",
                       mesh=mesh,
                       value=numerix.array([[0.], [0.]]),
                       elementshape=(2,))
Chi = CellVariable(name="Fission Chi",
                   mesh=mesh,
                   value=numerix.array([[0.], [0.]]),
                   elementshape=(2,))
nu = CellVariable(name="nu",
                  mesh=mesh,
                  value=numerix.array([[0.], [0.]]),
                  elementshape=(2,))
mu_bar = CellVariable(name="mu_bar",
                      mesh=mesh,
                      value=numerix.array([[0.], [0.]]),
                      elementshape=(2,))
Sigma_gh = numerix.array([[0., 0.], [0., 0.]])
Sv = CellVariable(name="source",
                  mesh=mesh,
                  value=numerix.array([[0.0], [0.0]]),
                  elementshape=(2,))  # external neutron source
Sigma_ggh = CellVariable(name="Outscatter",
                         mesh=mesh,
                         value=Sigma_gh,
                         elementshape=(2, 2,))
Sigma_hgg = CellVariable(name="Inscatter",
                         mesh=mesh,
                         value=numerix.flipud(Sigma_gh).T,
                         elementshape=(2, 2))

# Assign a material for each physical mesh region
for key, val in materialDict.iteritems():
    physicalMaterial = materialDict[key]
    physicalRegion = regionDict[key]
    Sigma_s.setValue(physicalMaterial.macroProp['Nsigma_s'], where=physicalRegion)
    Sigma_a.setValue(physicalMaterial.macroProp['Nsigma_a'], where=physicalRegion)
    Sigma_f.setValue(physicalMaterial.macroProp['Nsigma_f'], where=physicalRegion)
    Chi.setValue(physicalMaterial.macroProp['Chi'], where=physicalRegion)
    nu.setValue(physicalMaterial.macroProp['nu'], where=physicalRegion)
    mu_bar.setValue(physicalMaterial.macroProp['mu_bar'], where=physicalRegion)
    Sigma_ggh.setValue(numerix.array([physicalMaterial.macroProp['Nsigma_gh']]).T, where=physicalRegion)
    Sigma_hgg.setValue(numerix.array([numerix.flipud(physicalMaterial.macroProp['Nsigma_gh'])]).T, where=physicalRegion)

# Define diffusion coeff
# only valid if absorption is << total cross section
lambda_tr = 1.0 / ((Sigma_s + Sigma_a) - mu_bar * Sigma_s)
D = lambda_tr / 3.0  # [cm]

# Define solution variables
phi = CellVariable(name="phi",
                   mesh=mesh,
                   value=numerix.array([[phi0], [phi0]]),
                   elementshape=(2,),
                   hasOld=1)
k = CellVariable(name="keff",
                 mesh=mesh,
                 value=k0)

# Apply Boundary conditions
d = 0.71 * lambda_tr
dphidr_rmax = (1.0 / d) * phi  # only valid if d << Rmax, or planar wall
#phi.faceGrad.constrain(-dphidr_rmax, where=mesh.physicalFaces["exterior"])  # vacuume boundary condition
phi.constrain([[0.], [0.]], where=mesh.exteriorFaces)

# Neutron balance
# 0 = scatter_in/out - absorptions + fission neutrons + other Sources
Sa = phi * (Sigma_a + Sigma_f)
Sf = phi * Sigma_f * nu
Sf = Chi * numerix.sum(Sf, axis=0)
Outscatter = numerix.dot(phi, Sigma_ggh)
Inscatter = numerix.dot(phi, Sigma_hgg)
eq = DiffusionTerm(coeff=(-numerix.eye(2)), var=phi) + (Sa + Outscatter - Inscatter) / D == ((1.0 / k) * Sf + Sv) / D

# Iterate on flux and k until neutron balance is achived
#mysolver = LinearGMRESsolver(tolerance=1e-12, iterations=1000)
mysolver = GeneralSolver(tolerance=1e-6, iterations=500, precon=None)
kLax = 1.0
uLax = 0.7
o = 0
Ef = 3.2043e-11  # J/fission
Pwr = 1.e3
# Outter Power Iterations:
print("Solver Start.")
phi.setValue(phi * Pwr / numerix.sum(numerix.sum(phi * Sigma_f, axis=0) * Ef * mesh.cellVolumes))
while o <= 10:
    i, res, resOld = 0, 1.0e10, 0.1
    phi.setValue(phi * Pwr / numerix.sum(numerix.sum(phi * Sigma_f, axis=0) * Ef * mesh.cellVolumes))
    oldS = numerix.sum(numerix.sum(phi.old * Sigma_f, axis=0) * mesh.cellVolumes)
    # Inner flux iterations
    while res > 1.0e-8 and i < 5:
        phi.updateOld()
        res = eq.sweep(var=phi, solver=mysolver, underRelaxation=uLax)
        i += 1
        normRes = abs((resOld - res) / resOld)
        if normRes < 1e-5 and i > 2:
            break
        resOld = res
    print("Inner Iters Performed: %d, Flux Residual: %f,  k-eff: %f " % (i, normRes, numerix.average(k)))
    S = numerix.sum(numerix.sum(phi * Sigma_f, axis=0) * mesh.cellVolumes)
    o += 1
    k.value = (kLax * k.globalValue * S / oldS) + (1 - kLax) * k.globalValue
print("Total Outers Performed: %d " % o)
print("Reactor Power: %f " % numerix.sum(numerix.sum(phi * Sigma_f, axis=0) * Ef * mesh.cellVolumes))
print("The final k-eff estimate is : %f" % numerix.average(k))
viewer = Viewer(phi[0])
viewer2 = Viewer(phi[1])
viewer.plot()
viewer2.plot()
import pdb; pdb.set_trace()  # XXX BREAKPOINT

INTRODUCTION
============

DiFiPy is (will be) a collection of tools to solve 1-D, 2-D, and 3-D neutron
diffusion problems.

PLANED FEATURES
===============

- Geometry/Mesh import (via FiPy/Gmsh)
- Material and BC assignment
- Eigenvalue solver (quasi-steady state solver)
- Time dependent solver
- NJOY groupr wrapper
- N-group cross section lib
- Self shielding 


DEPENDENCIES
============

Python
------
- Numpy
- FiPy (Dev branch build from source.  github.com/usnistgov/fipy )
- Pysparse 
- Py-trilinos
- Matplotlib
- PyAMG (optional multigrid preconditioners)
- Mayavi2 (optional for 3-D plotting)
- Periodictable (optional for additional data)


Non-Python
----------
- NJOY99 (optional for rolling your own N-grp xsec libs)


CONTACT
=======
william.gurecky@gmail.com

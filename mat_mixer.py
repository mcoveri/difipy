#!/usr/bin/python


from fipy import *
from periodictable import *

# CONSTANTS
# -------------
#

# material properties library
# Store group constants for each energy group.
matLib = {}
# single group cross sections
matLib['U234'] = {'sigma_f': numerix.array([0.01045]),
                     'sigma_a': numerix.array([2.4]),
                     'sigma_s': numerix.array([9.3]),
                     'nu': numerix.array([2.4]),
                     'M': 234.0409456,  # Atomic mass [kg/mol]
                     'A': 92.0,   # Number of protons
                     'zaid': '92234'}
matLib['U235'] = {'sigma_f': numerix.array([500.0]),
                     'sigma_a': numerix.array([500.0 + 86.6]),
                     'sigma_s': numerix.array([14.9]),
                     'nu': numerix.array([2.4]),
                     'M': 235.043931,
                     'A': 92.0,
                     'zaid': '92235'}
matLib['U236'] = {'sigma_f': numerix.array([61.29]),
                     'sigma_a': numerix.array([5.0]),
                     'sigma_s': numerix.array([8.]),
                     'nu': numerix.array([2.4]),
                     'M': 236.0455619,
                     'A': 92.0,
                     'zaid': '92236'}
matLib['U238'] = {'sigma_f': numerix.array([0.01045]),
                     'sigma_a': numerix.array([2.4]),
                     'sigma_s': numerix.array([9.3]),
                     'nu': numerix.array([2.4]),
                     'M': 238.0507826,
                     'A': 92.0,
                     'zaid': '92238'}
matLib['H1'] = {'sigma_f': numerix.array([0.0]),
                   'sigma_a': numerix.array([0.294]),
                   'sigma_s': numerix.array([20.47]),
                   'M': 1.007825,
                   'A': 1.0,
                   'zaid': '1001'}
matLib['O16'] = {'sigma_f': numerix.array([0.0]),
                    'sigma_a': numerix.array([0.2]),
                    'sigma_s': numerix.array([3.78]),
                    'M': 15.9949146,  # Atomic mass [kg/mol]
                    'A': 8.0,   # Number of protons
                    'zaid': '8016'}
energyUBound = numerix.array([1.0e2, 1.0e-6])  # MeV energy upper group bounds
# Two group cross sections
# Group 1 (fast) group N (thermal)
# g is "current" group
# h is "other groups
matLib['U235_2g'] = {'sigma_f': numerix.array([[40.], [689.0]]),
                        'sigma_a': numerix.array([[48.], [780.6]]),
                        'sigma_s': numerix.array([[10.0], [15.1]]),  # scattering within the group
                        # scattering out of group g:
                        # for h =/= g
                        # phi_g * Sigma_gh ==> rate at which neutrons leave
                        # group g into group h
                        'sigma_gh': numerix.array([[0.01, 0.0],  # group 1 (fast) into grp2 (therm)
                                                   [0.0, 0.000]]),  # group 2 (therm) upscatter into grp1
                        'nu': numerix.array([[2.5], [2.5]]),
                        'Chi': numerix.array([[0.99], [0.01]]),  # probability of fission neutron being born in group g
                        'M': 235.0,
                        'A': 92.0,
                        'zaid': '92235'}
matLib['U238_2g'] = {'sigma_f': numerix.array([[0.3], [0.05045]]),
                        'sigma_a': numerix.array([[6.0], [3.4]]),
                        'sigma_s': numerix.array([[1.8], [9.3]]),
                        'sigma_gh': numerix.array([[0.007, 0.0],
                                                   [0.0, 0.000]]),
                        'nu': numerix.array([[2.5], [2.4]]),
                        'Chi': numerix.array([[0.95], [0.05]]),
                        'M': 238.0,
                        'A': 92.0,
                        'zaid': '92238'}
matLib['H1_2g'] = {'sigma_f': numerix.array([[0.0], [0.0]]),
                      'sigma_a': numerix.array([[0.03], [0.150]]),
                      'sigma_s': numerix.array([[16.0], [40.12]]),
                      'sigma_gh': numerix.array([[1.55, 0.0],
                                                 [0.0, 0.000]]),
                      'nu': numerix.array([[0.], [0.]]),
                      'Chi': numerix.array([[0.], [0.]]),
                      'M': 1.0,
                      'A': 1.0,
                      'zaid': '1001'}
matLib['O16_2g'] = {'sigma_f': numerix.array([[0.0], [0.0]]),
                       'sigma_a': numerix.array([[0.002], [0.001]]),
                       'sigma_s': numerix.array([[3.0], [4.12]]),
                       'sigma_gh': numerix.array([[0.045, 0.0],
                                                  [0.0, 0.000]]),
                       'nu': numerix.array([[0.], [0.]]),
                       'Chi': numerix.array([[0.], [0.]]),
                       'M': 16.0,
                       'A': 8.0,
                       'zaid': '8016'}


def updateAtomicMass(matLib=matLib):
    ''' Update atomic masses and atomic number from external source '''
    for key in matLib.keys():
        atM = matLib[key]['zaid'][2:]
        if len(matLib[key]['zaid']) == 4:
            aN = matLib[key]['zaid'][0]
        else:
            aN = matLib[key]['zaid'][:2]
        matLib[key]['M'] = elements[int(aN)][int(atM)].mass
        matLib[key]['A'] = float(elements[int(aN)][int(atM)].number)
    return matLib
matLib = updateAtomicMass(matLib)


class mixedMat(object):
    '''
    Smeared material class.
    Computes macroscopic cross sections, diffusion coeffs, ect.
    Input number densities in #/b-cm
    Input cross sections in barns
    '''
    Na = 6.02214129e23  # avagadros constant

    def __init__(self, ndDict, wfFlag=False, matLib=matLib):
        # Initilize mixedMat dicts
        self.macroProp = {}
        self.nDdict = {}
        self.wf = {}
        self.af = {}
        self.afF = {}
        # Pull requested isotopes from material library
        try:
            materialData = {k: matLib[k] for k in ndDict.keys()}
        except:
            print("Material __ not found in material library")
        self.microDat = materialData
        # If weight fracs are given, convert to atom frac and proceed as usual
        if wfFlag:
            self.wf = ndDict
            self.nD = self.__wfToAf()
        else:
            self.nD = ndDict
        # Compute properties
        self.__numberDensityTable()
        self.__updateDB()

    def __updateDB(self):
        self.macroProp = {}
        self.__computeAtomicFracs()
        self.__computeMacroXsec()
        self.__computeAvgProps()
        self.__computeDensity()
        self.__afToWf()

    def __numberDensityTable(self):
        '''
        Asscociate each isotope with number density.
        '''
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            self.nDdict[material] = self.nD[material]

    def __computeAtomicFracs(self):
        ''' Compute atomic fractions.  Usefull if material was specified by
        atom density or weight fractions '''
        fissionable_nD_sum = 0
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            self.af[material] = self.nDdict[material] / sum(self.nDdict.values())
            if 'nu' in data.keys():
                if sum(data['nu']) > 0:
                    fissionable_nD_sum += self.nDdict[material]
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            if 'nu' in data.keys():
                try:
                    self.afF[material] = self.nDdict[material] / fissionable_nD_sum
                except:
                    self.afF[material] = 0.0

    def __wfToAf(self):
        '''
        Converts weight fractions to atom fracs
        [g/gtot * mol/g * atoms/mol]  / atoms tot
        assuming gtot = 1
        '''
        atomSum = 0
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            atomSum += self.wf[material] * (1. / self.microDat[material]['M']) * self.Na
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            self.af[material] = self.wf[material] * (1. / self.microDat[material]['M']) * self.Na / atomSum
        return self.af

    def __afToWf(self):
        '''
        Converts atom fractions to weight fractions
        [atoms/atomstot * atomstot * mol/atom * g/mol] / g tot
        assuming atomstot = 1
        '''
        if hasattr(self, 'af'):
            wfSum = 0
            for i, (material, data) in enumerate(self.microDat.iteritems()):
                wfSum += self.af[material] * (1 / self.Na) * self.microDat[material]['M']
            for i, (material, data) in enumerate(self.microDat.iteritems()):
                self.wf[material] = self.af[material] * (1 / self.Na) * self.microDat[material]['M'] / wfSum
        else:
            self.__computeAtomicFracs()
            self.__afToWf()
        return self.wf

    def __computeMacroXsec(self):
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            for key, value in data.iteritems():
                if key in ['sigma_f', 'sigma_a', 'sigma_s', 'sigma_gh']:
                    try:
                        self.macroProp['N' + key] += self.nDdict[material] * value
                    except:
                        self.macroProp['N' + key] = self.nDdict[material] * value
                else:
                    pass

    def __computeAvgProps(self):
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            for key, value in data.iteritems():
                if key in ['M']:
                    try:
                        self.macroProp[key] += self.af[material] * value
                    except:
                        self.macroProp[key] = self.af[material] * value
                elif key in ['nu', 'Chi']:
                    try:
                        self.macroProp[key] += self.afF[material] * value
                    except:
                        self.macroProp[key] = self.afF[material] * value
                elif key in ['A']:
                    try:
                        self.macroProp[key] += self.af[material] * value
                        self.macroProp['mu_bar'] += self.af[material] * (2.0 / (3.0 * value))
                    except:
                        self.macroProp[key] = self.af[material] * value
                        self.macroProp['mu_bar'] = self.af[material] * (2.0 / (3.0 * value))
                else:
                    pass

    def __checkMaterialLib(self):
        '''
        Checks the incomming material libs for missing data.
        '''
        #requiredData = ['sigma_a', 'sigma_s', 'M', 'A']
        pass

    def __isFissile(self):
        ''' extra data for fissile materials '''

        pass

    def __computeDensity(self):
        self.density = sum(self.macroProp['M'] * numerix.array(self.nDdict.values()) * 1.0e24 /
                           self.Na / numerix.array(self.af.values())) / len(self.af.values())

    def setDensity(self, density):
        ''' Supply material density in g/cc.  Updates atom densities accordingly. '''
        self.density = density  # g/cc
        # need keep atomic ratios the same, but update atom densities
        # such that the density specified is met
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            # g/cc * #/mol * mol/g
            self.nDdict[material] = density * self.af[material] * self.Na / self.macroProp['M'] / 1.0e24
        self.__updateDB()

    def __add__(self, other):
        '''
        Allows the mixing of predifined mixtures by mass:
        mix 0.2 U238 (by mass) with 0.8 lwtr (by mass) where the
        U238 and lwtr materials were constructed by mixing individual
        isotopes.
        '''
        # add number densities together
        for material, nDensity in other.nDdict.iteritems():
            try:
                self.nDdict[material] += nDensity
            except:
                # We've got a new isotope on our hands!
                self.nDdict[material] = nDensity
                self.microDat[material] = other.microDat[material]
        # update macro property database
        self.__updateDB()
        return self

    def __mul__(self, const):
        '''
        Multiplication by weight fraction.
        '''
        if hasattr(self, 'density'):
            self.setDensity(self.density * const)
        else:
            print("Warning: must specify material density before weighting by mass fraction")
        return self

    def __rmul__(self, const):
        return self.__mul__(const)


if __name__ == "__main__":
    ## create the smeared moderator fuel mixture
    #fuelMat = mixedMat({k: materials[k] for k in ('U235', 'U238')},  # materials
    #                   {'U235': 0.005, 'U238': 0.995})  # number densities or atomic fractions
    #fuelMat.setDensity(10.2)  # g/cc
    #lwtrMat = mixedMat({k: materials[k] for k in ('O16', 'H1')},
    #                   {'O16': 1.0 / 3.0, 'H1': 2.0 / 3.0})
    #lwtrMat.setDensity(1.0)  # g/cc
    #coreMat = lwtrMat * 0.7 + fuelMat * 0.3
    ## Another test case
    #leuMat = mixedMat({k: materials[k] for k in ('U234', 'U235', 'U236', 'U238')},
    #                   {'U234': 6.11864e-06,
    #                    'U235': 0.000718132,
    #                    'U236': 3.29861e-06,
    #                    'U238': 0.0221546})
    #print leuMat.density
    leuMat = mixedMat({'U234': 6.11864e-06,
                       'U235': 0.000718132,
                       'U236': 3.29861e-06,
                       'U238': 0.0221546})
    print leuMat.density

    fuelMat = mixedMat({'U235': 0.005, 'U238': 0.995})
    fuelMat.setDensity(10.2)  # g/cc
    lwtrMat = mixedMat({'O16': 1.0 / 3.0, 'H1': 2.0 / 3.0})
    lwtrMat.setDensity(1.0)  # g/cc
    coreMat = lwtrMat * 0.7 + fuelMat * 0.3
    print coreMat.density
    # use core mat weight fractions to seed a new material and check for
    # consitancy
    checkMat = mixedMat(coreMat.wf, wfFlag=True)
    print coreMat.density
    print coreMat.nDdict

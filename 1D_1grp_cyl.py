#!/usr/bin/python

# Finite Volume approach to neutron diffusion in a
# 1D cylendrical geometry.
#
# Steady state case considered (k=1)
# TODO: quasi steady state case
#
# William Gurecky
#
# Changelog
# -------------
#
#
#

from fipy import *
from mat_mixer import *
import pylab as pl

# CONSTANTS
# -------------

# create the smeared moderator fuel mixture
fuelMat = mixedMat({'U235': 0.02, 'U238': 0.97})
fuelMat.setDensity(10.2)  # g/cc
lwtrMat = mixedMat({'O16': 1.0 / 3.0, 'H1': 2.0 / 3.0})
lwtrMat.setDensity(1.0)  # g/cc
coreMat = lwtrMat * 0.6 + fuelMat * 0.4

# absorption and fission
Sigma_f = coreMat.macroProp['Nsigma_f']   # [barns] * 1.0e-24 fission cross section [cm]
nu = coreMat.macroProp['nu']   # average neutrons / fission
Sigma_a = coreMat.macroProp['Nsigma_a']

# scattering
Sigma_s = coreMat.macroProp['Nsigma_s']
mu_bar = coreMat.macroProp['mu_bar']

# Define diffusion coeff
# only valid if absorption is << total cross section
lambda_tr = 1.0 / (Sigma_s - mu_bar * Sigma_s)
D = lambda_tr / 3.0  # [cm]

# geometry
Lr = 14.0  # [cm] max radius

# setup the mesh
nr = 50
mesh = CylindricalGrid1D(Lx=Lr, nx=nr, overlap=4, communicator=parallelComm)
# Store locations of mesh centers and faces
rc = mesh.cellCenters
rfc = mesh.faceCenters

# Initial guess for flux (phi) and neutron multiplication factor
phi0 = 1.0e5  # arbitrary flux
k0 = 1.01

# Define solution variables
phi = CellVariable(name="phi",
                   mesh=mesh,
                   value=phi0,
                   hasOld=1)

Sv = CellVariable(name="source",
                  mesh=mesh,
                  value=0.0)
k = CellVariable(name="keff",
                 mesh=mesh,
                 value=k0)
#k = Variable(name="keff",
#             value=k0)

# Apply Boundary conditions
d = 0.71 * lambda_tr
dphidr_rmax = (phi[-1] / d)  # only valid if d << Rmax, or planar wall
phi.faceGrad.constrain([-dphidr_rmax], where=mesh.facesRight)  # vacuume boundary condition
#phi.constrain(0., where=mesh.facesRight)

# Neutron balance
# 0 = scatter_in/out - absorptions + fission neutrons + other Sources
Sa = phi * (Sigma_a + Sigma_f)
Sf = phi * Sigma_f * nu  # * 8.
eq = DiffusionTerm(coeff=-D, var=phi) + Sa == (1.0 / k) * Sf + Sv

# Iterate on flux and k until neutron balance is achived
kLax = 1.0
uLax = 0.95
o = 0
# Outter Power Iterations
while o <= 3:
    i, res, resOld = 0, 1.0e10, 0.0
    phi.setValue(phi / numerix.sum(phi * Sigma_f * mesh.cellVolumes))  # normalize flux
    phi.updateOld()
    oldS = sum(Sf.old.globalValue)
    #oldS = sum(Sf.old)
    # Inner flux iterations
    while res > 1.0e-6 and i < 200:
        res = eq.sweep(var=phi, underRelaxation=uLax)
        phi.setValue(phi / numerix.sum(phi * Sigma_f * mesh.cellVolumes))  # normalize flux
        i += 1
        if abs(resOld - res) < 1e-10:
            break
        resOld = res
    print("Inner Iters Performed: %d, Flux Residual: %f,  k-eff: %f " % (i, res, numerix.average(k)))
    S = sum(Sf.globalValue)
    #S = sum(Sf)
    o += 1
    #k.value = (kLax * k.globalValue * S / oldS) + (1 - kLax) * k.globalValue
    k.value = (kLax * k * S / oldS) + (1 - kLax) * k
print("Total Outers Performed: %d " % o)
print("The final k-eff estimate is : %f" % numerix.average(k))
Pwr = 1.e6  # [W]
pl.figure(1)
pl.plot(rc.value[0], ((Pwr / 3.204343e-11) * phi * numerix.sum((phi * Sigma_f * mesh.cellVolumes))).value)
pl.xlabel("Radial distance [cm]")
pl.ylabel("Flux [n/cm^2-s]")
pl.show()

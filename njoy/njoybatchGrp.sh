#!/bin/bash
#
# NJOY thermr, broadr and groupr batch script
# used to doppler broaden resonance peaks, weight
# cross sections by custom neutron spectrum prodeuced
# by CE MC run.
# Output intended for use in diffusion computations
#
# Author:
# William Gurecky
# william.gurecky@utexas.edu
#
# Change Log:
# ------------
# 7/27/2014  Creation
#
# notes:

# Supply desired output material directory
MATDIR=matdir
mkdir $MATDIR

# Supply desired temperature
TEMP='555.'
echo 'NJOY run at' $TEMP 'K'

# Set folder that contains endf data files
# to be processed
FILES=/data/wlg333/oklo/spring2014/njoy/endfvii/*

# Run NJOY
for file in $FILES
do
MAT=`awk '/VII[ ]+MATERIAL/ {print $3}' $file `
echo 'Material: ' $MAT
echo 'Getting endf tape...'
cp $file tape20
echo 'running njoy'
cat>input <<EOF
 moder
 20 -21
 reconr
 -21 -22
 'pendf tape from endf/b-vii tape 21'/
 $MAT 3/
 .005/
 'from endf/b tape 20'/
 'processed by the njoy nuclear data processing system'/
 'see original endf/b-vii tape for details of evaluation'/
 0/
 broadr
 -21 -22 -23
 $MAT 1 0 1 0/
 .005/
 $TEMP
 0/
 unresr
 -21 -23 -24
 $MAT 1 7 1
 $TEMP
 1.e10 1.e5 1.e3 100. 10. 1 .1
 0/
 thermr
 0 -24 -25
 0 $MAT 8 1 1 0 0 1 221 0
 $TEMP
 .05 4.2
 groupr
 -21 -25 0 26
 $MAT 1 0 4 1 1 5 1
 'Group constants'/
 $TEMP
 1.e10 3000. 1000. 200. 50. /
 2/
 1e-5 4.3e-2 10e6 /
 0.1 0.025 820.3e3 1.4e6 /
 3 1 'total'/
 3 2 'elastic'/
 3 18 'fission'/
 3 102 'radCap'/
 3 251 'mu bar'/
 3 452 'nu bar'/
 6 2 'ggh escatter'/
 6 51 'ggh iscatter'/
 6 -66/
 0/
 0/
 stop
EOF
xnjoy<input

# Write outputs
echo 'saving output gout file'
mv tape26 $MATDIR/grpConst_$MAT
done

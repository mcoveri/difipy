#!/usr/bin/python

# Extracts group constants from NJOY GROUPR output tape.
# Usefull for building a nuclear material property database
# that can be imported by a diffusion theory code to predict
# nuclear performance of simple reactor geometries.
#
# changelog
# ---------
#

import re
from astropy.io import ascii

# Constains key phrases that mark the start and end of the desired
# information.
endMark = '\d+\.?\d?s'
regexDict = {
    'temp': ['\stemperatures\s\(kelvin\).*', 'sigma'],
    'sigma_zeros': ['\ssigma\szeroes', 'neutron'],
    'groups': ['neutron\sgroup\structure.*read', 'weight'],
    'totalXS': ['.*mf\s+3\sand\smt\s+1', endMark],
    'elasticXS': ['.*mf\s+3\sand\smt\s+2', endMark],
    'fissionXS': ['.*mf\s+3\sand\smt\s+18', endMark],
    'radcapXS': ['mt102', endMark],
    'muBar': ['mt251', endMark],
    'nuBar': ['mt452', endMark],
    'elasticMXS': ['escatter', endMark]
}


def fileToList(infile):
    ''' Convert text file to list of strings '''
    f1 = open(infile, 'r')
    filelines = [line for line in f1]
    f1.close()
    return filelines


def extractTable(infile, limits):
    table = []
    lines = infile[limits[0]:limits[1]]
    for tableLine in lines:
        table.append(tableLine)
    return table


def findTableLimits(filelines, tagStrings):
    limits = [0, 0]
    startLine, endLine = None, None
    for i, line in enumerate(filelines):
        if tagStrings[0].search(line):
            startLine = i
        elif tagStrings[1].search(line) and startLine:
            endLine = i
        if endLine and startLine:
            limits = [startLine, endLine]
            break
    return limits


def splitTable(asciiTableData):
    splitTable = []
    for line in asciiTableData:
        splitTable.append(line.split())
    return splitTable


def getAsciiTables(filelines):
    tableLimits = (0, 0)
    tables = {}
    for key, regexs in regexDict.iteritems():
        startStr = re.compile(regexs[0])
        endStr = re.compile(regexs[1])
        tagStrings = (startStr, endStr)
        # Extract ascii data that lies in between the 'start' and 'end' marks
        # for the table.  Store data tables in dict, labeled with their
        # corrosponding key.
        tableLimits = findTableLimits(filelines, tagStrings)
        asciiTableData = extractTable(filelines, tableLimits)
        try:
            tables[key] = ascii.read(asciiTableData[3:])
        except:
            try:
                tables[key] = ascii.read(asciiTableData[4:])
            except:
                print("Exception logic for key: " + key)
                tables[key] = splitTable(asciiTableData)
    return tables


if __name__ == "__main__":
    infile = './output'
    filelines = fileToList(infile)
    getAsciiTables(filelines)
